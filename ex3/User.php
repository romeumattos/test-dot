<?php
namespace Application;

use DatabaseConnection;

class User
{
    /* @var DatabaseConnection */
    private $connection;

    /**
     * User constructor.
     * @param DatabaseConnection $connection
     */
    public function __construct(DatabaseConnection $connection)
    {
        $this->connection = $connection;
    }

    public function getUserList()
    {
        $query = 'select name from user';
        $results = $this->connection->query($query);
        sort($results);

        return $results;
    }
}