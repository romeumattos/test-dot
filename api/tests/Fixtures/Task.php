<?php
/**
 * This file is part of Skel system
 *
 * @copyright Skel
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Fixtures;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Skel\Entity\Tasks\Task as TaskModel;

/**
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
class Task extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * Loader
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $i = 1;

        while ($i <= 10) {
            $task = [
                'id' => $i,
                'status' => TaskModel::ACTIVE,
                'title' => 'Task ' . $i,
                'description' => 'task' . $i . ' description',
                'priority' => 1
            ];

            $obj = new TaskModel();
            $obj->setTitle($task['title']);
            $obj->setDescription($task['description']);
            $obj->setPriority($task['priority']);
            $obj->setStatus($task['status']);

            $manager->persist($obj);
            $manager->flush();

            $this->addReference('task_' . $task['id'], $obj);

            $i++;
        }
    }

    /**
     * Load order
     *
     * @return int
     */
    public function getOrder()
    {
        return 1;
    }
}
