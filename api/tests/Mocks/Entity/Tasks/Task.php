<?php
/**
 * This file is part of Skel system
 *
 * @copyright Skel
 * @license   proprietary
 */
namespace Skel\Mocks\Entity\Tasks;

use Skel\Entity\Tasks\TaskInterface;
use Mockery as m;

/**
 * Task Entity Mock
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
trait Task
{
    public function taskMock()
    {
        $task = m::mock(TaskInterface::class);
        $task->shouldReceive('getId')->andReturn(rand())->byDefault();
        $task->shouldReceive('setStatus')->andReturn(true)->byDefault();
        $task->shouldReceive('setTitle')->andReturn(true)->byDefault();
        $task->shouldReceive('setDescription')->andReturn(true)->byDefault();
        $task->shouldReceive('getDescription')->andReturn(rand() . ' task description')->byDefault();

        return $task;
    }
}