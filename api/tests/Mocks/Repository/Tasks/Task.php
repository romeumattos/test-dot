<?php
/**
 * This file is part of Skel system
 *
 * @copyright Skel
 * @license   proprietary
 */
namespace Skel\Mocks\Repository\Tasks;

use Skel\Repository\Tasks\TaskInterface;
use Mockery as m;

/**
 * Task Entity Mock
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
trait Task
{
    public function taskRepositoryMock()
    {
        $task = m::mock(TaskInterface::class);
        $task->shouldReceive('findById')->andReturn($this->taskMock())->byDefault();

        return $task;
    }
}