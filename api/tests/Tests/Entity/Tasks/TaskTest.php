<?php
/**
 * This file is part of Skel system
 *
 * @copyright Skel
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Tests\Entity\Tasks;

use DateTime;
use Skel\Entity\Tasks\Task;
use Skel\Tests\ChangeProtectedAttribute;
use PHPUnit_Framework_TestCase;

/**
 * Task test case.
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
class TaskTest extends PHPUnit_Framework_TestCase
{
    /**
     * @see \Skel\Tests\ChangeProtectedAttribute
     */
    use ChangeProtectedAttribute;

    /**
     * @return multitype:multitype:number
     */
    public function validObjects()
    {
        $obj = new \stdClass();
        $obj->id = 1;
        $obj->created = (new DateTime());
        $obj->updated = (new DateTime())->modify('+1 day');
        $obj->status = Task::NEWER;
        $obj->title = 'Teste';
        $obj->description = 'teste@teste.net';
        $obj->priority = 2;

        return [
            [
                $obj
            ]
        ];
    }

    /**
     * @return multitype:multitype:number
     */
    public function invalidObjects()
    {
        $obj = new \stdClass();
        $obj->id = 'SS';
        $obj->created = (new DateTime())->modify('+3 day');
        $obj->updated = (new DateTime())->modify('-10 day');
        $obj->status = 'ok';
        $obj->title = '';
        $obj->description= '';
        $obj->priority = '';

        return [
            [
                $obj
            ]
        ];
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::getId
     */
    public function getIdShouldReturnTheIdAttribute($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'id', $obj->id);

        $this->assertEquals($task->getId(), $obj->id);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::getCreated
     */
    public function getCreateReturnCreateAttribute($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'created', $obj->created);

        $this->assertSame($task->getCreated(), $obj->created);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setCreated
     */
    public function setCreateReturnEmpty($obj)
    {
        $task = new Task();

        $result = $task->setCreated($obj->created);

        $this->assertEmpty($result);
    }

    /**
     * @test
     *
     * @dataProvider invalidObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setCreated
     *
     * @expectedException InvalidArgumentException
     */
    public function setCreateThrowsExceptionWhenDateOnTheFuture($obj)
    {
        $task = new Task();
        $task->setCreated($obj->created);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::getUpdated
     */
    public function getUpdateShouldReturnTheUpdateAttribute($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'updated', $obj->updated);

        $this->assertSame($obj->updated, $task->getUpdated());
    }

    /**
     * @test
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setUpdated
     */
    public function setUpdateReturnEmptyWhenSuccess($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'created', $obj->created);

        $return = $task->setUpdated($obj->updated, $obj->updated);

        $this->assertEmpty($return);
    }

    /**
     * @test
     *
     * @dataProvider invalidObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setUpdated
     *
     * @expectedException InvalidArgumentException
     */
    public function setUpdateThrowsExceptionWhenLowerThanCreate($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'created', $obj->created);

        $task->setUpdated($obj->updated);
    }

    /**
     * @test
     *
     * @dataProvider invalidObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setUpdated
     *
     * @expectedException InvalidArgumentException
     */
    public function setUpdateThrowsExceptionWhenCreateIsNull($obj)
    {
        $task = new Task();
        $task->setUpdated($obj->updated);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::getStatus
     */
    public function getStatusShouldReturnTheStatusAttribute($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'status', $obj->status);

        $this->assertEquals($obj->status, $task->getStatus());
    }

    /**
     * @test
     *
     * @expectedException InvalidArgumentException
     *
     * @dataProvider invalidObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setStatus
     */
    public function setStatusThrowsExceptionWhenNotIntegerStatus($obj)
    {
        $task = new Task();
        $task->setStatus($obj->status);
    }

    /**
     * @test
     *
     * @expectedException InvalidArgumentException
     *
     * @dataProvider invalidObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setStatus
     */
    public function setStatusThrowsExceptionWhenInvalidStatus($obj)
    {
        $task = new Task();
        $task->setStatus(1000);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setStatus
     */
    public function setStatusReturnsEmpty($obj)
    {
        $task = new Task();

        $result = $task->setStatus($task::ACTIVE);

        $this->assertempty($result);
    }

    /**
     * @test
     *
     * @covers Skel\Entity\Tasks\Task::isNewer
     */
    public function isNewerReturnTrueWhenObjectOnNewerStatus()
    {
        $task = new Task();

        $this->modifyAttribute($task, 'status', $task::NEWER);

        $this->assertTrue($task->isNewer());
    }

    /**
     * @test
     *
     * @covers Skel\Entity\Tasks\Task::isActive
     */
    public function isActiveReturnTrueWhenObjectOnActiveStatus()
    {
        $task = new Task();

        $this->modifyAttribute($task, 'status', $task::ACTIVE);

        $this->assertTrue($task->isActive());
    }

    /**
     * @test
     *
     * @covers Skel\Entity\Tasks\Task::isDeleted
     */
    public function isDeletedReturnTrueWhenObjectOnDeletedStatus()
    {
        $task = new Task();

        $this->modifyAttribute($task, 'status', $task::DELETED);

        $this->assertTrue($task->isDeleted());
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::delete
     */
    public function deleteReturnTrueWhenSuccess($obj)
    {
        $task = new Task();

        $result = $task->delete();

        $this->assertTrue($result);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::delete
     *
     * @expectedException InvalidArgumentException
     */
    public function deleteThrowsExceptionWhenAlreadyDeleted($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'status', Task::DELETED);

        $task->delete();
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::delete
     * @covers       Skel\Entity\Tasks\Task::isDeleted
     */
    public function isDeletedReturnTrueAfterDeleteMethod($obj)
    {
        $task = new Task();
        $task->delete();

        $this->assertTrue($task->isDeleted());
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::delete
     * @covers       Skel\Entity\Tasks\Task::isActive
     */
    public function isActiveReturnFalseAfterDeleteMethod($obj)
    {
        $task = new Task();
        $task->delete();

        $this->assertFalse($task->isActive());
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::delete
     * @covers       Skel\Entity\Tasks\Task::isNewer
     */
    public function isNewerReturnFalseAfterDeleteMethod($obj)
    {
        $task = new Task();
        $task->delete();

        $this->assertFalse($task->isNewer());
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setTitle
     */
    public function setTitleReturnEmptyOnSuccess($obj)
    {
        $task = new Task();

        $result = $task->setTitle($obj->title);

        $this->assertEmpty($result);
    }

    /**
     * @test
     *
     * @dataProvider invalidObjects
     *
     * @covers       Skel\Entity\Tasks\Task::setTitle
     *
     * @expectedException InvalidArgumentException
     */
    public function setTitleThrowsExceptionWhenEmpty($obj)
    {
        $task = new Task();
        $task->setTitle($obj->title);
    }

    /**
     * @test
     *
     * @dataProvider validObjects
     *
     * @covers       Skel\Entity\Tasks\Task::getTitle
     */
    public function getTitleReturnTitleAttribute($obj)
    {
        $task = new Task();

        $this->modifyAttribute($task, 'title', $obj->title);

        $this->assertEquals($task->getTitle(), $obj->title);
    }
}
