<?php
/**
 * This file is part of Skel system
 *
 * @copyright Skel
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Tests\Service\Tasks;

use Skel\Service\Tasks\Task as TaskService;
use Skel\Mocks\Repository\Tasks\Task as CreateTaskRepositoryMock;
use Skel\Mocks\Entity\Tasks\Task as CreateTaskEntityMock;
use Skel\Mocks\Database as CreateEmMock;
use Skel\Tests\ChangeProtectedAttribute;
use PHPUnit_Framework_TestCase;

/**
 * Task service test case.
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
class TaskTest extends PHPUnit_Framework_TestCase
{
    use CreateEmMock;
    use ChangeProtectedAttribute;
    use CreateTaskRepositoryMock;
    use CreateTaskEntityMock;

    /**
     * @var TaskService
     */
    private $service;

    /**
     * Bootstrap
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->service = new TaskService($this->getDefaultEmMock(), $this->taskRepositoryMock());
    }

    /**
     * Shutdown
     *
     * @return void
     */
    public function tearDown()
    {
        $this->service = null;

        parent::tearDown();
    }

    /**
     * @test
     * @covers Skel\Service\Tasks\Task::save()
     */
    public function save()
    {
        $taskModel = $this->taskMock();

        $result = $this->service->save($taskModel);

        $this->assertSame($taskModel, $result);
    }

    /**
     * @test
     * @covers Skel\Service\Tasks\Task::delete()
     */
    public function delete()
    {
        $taskModel = $this->taskMock();

        $result = $this->service->delete($taskModel);

        $this->assertTrue($result);
    }
}
