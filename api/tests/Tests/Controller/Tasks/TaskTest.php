<?php
/**
 * @author Thiago Paes <mrprompt@gmail.com>
 * @copyright free
 */
namespace Skel\Tests\Controller\Tasks;

use Skel\Tests\ApplicationTestCase;

/**
 * Task Controller test case
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
class TaskTest extends ApplicationTestCase
{
    /**
     * Data Provider
     *
     * @return multitype:multitype:multitype:string
     */
    public function validObjects()
    {
        return [
            [
                [
                    "title" => 'Test title',
                    "description" => 'test description',
                    "priority" => 3,
                ]
            ],
        ];
    }

    /**
     * @test
     */
    public function getAllTasks()
    {
        $client = $this->createClient();
        $client->request('GET', '/task/', [], [], $this->header);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getTaskById()
    {
        $client = $this->createClient();
        $client->request('GET', '/task/1', [], [], $this->header);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function getTaskByIdThrowsExceptionWhenTaskIsInvalid()
    {
        $client = $this->createClient();
        $client->request('GET', '/task/0', [], [], $this->header);

        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function putTaskWithFullParameters($obj)
    {
        $client = $this->createClient();
        $client->request('PUT', '/task/1', $obj, [], $this->header);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function putTaskWithoutParameters()
    {
        $client = $this->createClient();
        $client->request('PUT', '/task/', [], [], $this->header);

        $this->assertEquals(405, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function deleteTaskWithoutParameters()
    {
        $client = $this->createClient();
        $client->request('DELETE', '/task/', [], [], $this->header);

        $this->assertEquals(405, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     */
    public function deleteTask()
    {
        $client = $this->createClient();
        $client->request('DELETE', '/task/1', [], [], $this->header);

        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function postTaskWithFullParameters($obj)
    {
        $client = $this->createClient();
        $client->request('POST', '/task/', $obj, [], $this->header, json_encode($obj));

        $this->assertEquals(201, $client->getResponse()->getStatusCode());
    }

    /**
     * @test
     * @dataProvider validObjects
     */
    public function postTaskWithoutParameters($obj)
    {
        $client = $this->createClient();
        $client->request('POST', '/task/', [], [], $this->header);

        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }
}
