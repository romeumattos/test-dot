<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Controller\Tasks;

use Skel\Bootstrap as Application;
use Skel\Response as View;
use Skel\Service\Tasks\Task as TaskService;
use Skel\Factory\Tasks\Task as TaskFactory;

/**
 * Task Controller
 *
 * @author Romeu Mattos0 <romeu.smattos@gmail.com>
 */
class Task implements TaskInterface
{
    /**
     * GET Request for /task/
     *
     * @param  Application $app
     * @return View
     */
    public function get(Application $app)
    {
        $tasks = $app[TaskService::NAME]->findAll();

        return new View($tasks, View::HTTP_OK);
    }

    /**
     * GET request for /task/{id}
     *
     * @param  Application $app
     * @return View
     */
    public function view(Application $app)
    {
        $task = $app[TaskService::NAME]->findById($app['request']->get('id'));

        return new View($task, View::HTTP_OK);
    }

    /**
     * PUT request route for /task/{id}
     *
     * @param  Application $app
     * @return View
     */
    public function update(Application $app)
    {
        $app['task'] = $app[TaskService::NAME]->findById($app['request']->get('id'));

        $task = $app[TaskService::NAME]->update($app['task'], $app['request']);

        return new View($task, View::HTTP_NO_CONTENT);
    }

    /**
     * DELETE request route for /task/{id}
     *
     * @param  Application $app
     * @return View
     */
    public function delete(Application $app)
    {
        $app['task'] = $app[TaskService::NAME]->findById($app['request']->get('id'));

        $task = $app[TaskService::NAME]->delete($app['task']);

        return new View($task, View::HTTP_NO_CONTENT);
    }

    /**
     * Create an task
     *
     * @param  Application $app
     * @param  Request $app['request']
     * @return View
     */
    public function create(Application $app)
    {
        $app['task'] = TaskFactory::create($app['request']);

        $task = $app[TaskService::NAME]->save($app['task']);

        return new View($task, View::HTTP_CREATED);
    }
}
