<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Controller\Tasks;

use Skel\Bootstrap as Application;

/**
 * Task Controller Interface
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
interface TaskInterface
{
    /**
     * @param  Application $app
     * @return View
     */
    public function get(Application $app);

    /**
     * @param  Application $app
     * @return View
     */
    public function view(Application $app);

    /**
     * @param  Application $app
     * @return View
     */
    public function update(Application $app);

    /**
     * @param  Application $app
     * @return View
     */
    public function delete(Application $app);

    /**
     * @param  Application $app
     * @return View
     */
    public function create(Application $app);
}
