<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Factory\Tasks;

use Skel\Entity\Tasks\Task as TaskModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * The Task Factory
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
abstract class Task
{
    /**
     *
     * @param Request $task
     * @return TaskModel
     */
    public static function create(Request $task)
    {
        $obj = new TaskModel();
        $obj->setTitle($task->get('title'));
        $obj->setDescription($task->get('description'));
        $obj->setPriority($task->get('priority'));
        $obj->setStatus(TaskModel::NEWER);

        return $obj;
    }

    /**
     *
     * @param TaskModel $task
     * @param Request $req
     * @return TaskModel
     */
    public static function update(TaskModel $task, Request $req)
    {
        $task->setTitle($req->get('title'));
        $task->setDescription($req->get('description'));
        $task->setPriority($req->get('priority'));
        $task->setStatus(TaskModel::ACTIVE);

        return $task;
    }
}
