<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Repository\Tasks;

use Skel\Entity\Tasks\Task as TaskModel;
use Skel\Repository\Repository;
use OutOfRangeException;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Thiago Paes <mrprompt@gmail.com>
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
class Task extends Repository implements TaskInterface
{
    /**
     * @see \Skel\Repository\Repository::findAll()
     */
    public function findAll()
    {
        $tasks = $this->em->getRepository('Skel\Entity\Tasks\Task')->findBy([
            'status' => [
                TaskModel::NEWER,
                TaskModel::ACTIVE
            ]
        ]);

        return $tasks;
    }

    /**
     * @see \Skel\Repository\Repository::findById()
     */
    public function findById($id)
    {
        $task = $this->em->getRepository('Skel\Entity\Tasks\Task')->findOneBy([
            'id' => $id,
            'status' => [
                TaskModel::NEWER,
                TaskModel::ACTIVE
            ]
        ]);

        if (null === $task || $task->isDeleted()) {
            throw new OutOfRangeException('Task not found', Response::HTTP_NOT_FOUND);
        }

        return $task;
    }
}
