<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Repository\Tasks;

use Skel\Repository\Repository;

/**
 * Task Repository Interface
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
interface TaskInterface
{

}
