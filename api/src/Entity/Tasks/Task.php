<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Entity\Tasks;

use Doctrine\ORM\Mapping as ORM;
use Skel\Entity\Entity as Persistable;
use InvalidArgumentException;
use Respect\Validation\Exceptions\AllOfException;
use Respect\Validation\Validator as v;
use JMS\Serializer\Annotation\Exclude;

/**
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 *
 * @ORM\Table(
 *      name="task",
 *      indexes={
 *          @ORM\Index(name="task_index_id", columns={"id"}),
 *      }
 * )
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Task extends Persistable implements TaskInterface
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Exclude
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="title", type="string", length=1024, nullable=false)
     * @var string
     */
    protected $title;

    /**
     * @ORM\Column(name="description", type="string", length=1024)
     * @var string
     */
    protected $description;

    /**
     * @ORM\Column(name="priority", type="smallint", nullable=true, options={"default" = 1})
     *
     * @var int
     */
    protected $priority;


    /**
     * Set Title
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function setTitle($title = '')
    {
        try {
            v::notEmpty()->alnum()->assert($title);

            $this->title= $title;
        } catch (AllOfException $e) {
            throw new InvalidArgumentException(sprintf('Title %s is invalid', $title), 500, $e);
        }
    }

    /**
     * Get the Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Description
     *
     * @return void
     */
    public function setDescription($description = '')
    {
        $this->description = $description;
    }

    /**
     * Get the Description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set Priority
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function setPriority($priority)
    {
        try {
            v::int($priority);

            $this->$priority= $priority;
        } catch (AllOfException $e) {
            throw new InvalidArgumentException(sprintf('Priority %s is invalid', $priority), 500, $e);
        }
    }

    /**
     * Get the Priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
