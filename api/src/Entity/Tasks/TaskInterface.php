<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Entity\Tasks;

/**
 * Task Entity Interface
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
interface TaskInterface
{
    /**
     * Set Title
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function setTitle($title = '');

    /**
     * Get the Title
     *
     * @return string
     */
    public function getTitle();

    /**
     * Set Description
     *
     * @return void
     */
    public function setDescription($description = '');

    /**
     * Get the Description
     *
     * @return string
     */
    public function getDescription();

    /**
     * Set Priority
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function setPriority($priority);

    /**
     * Get the Priority
     *
     * @return int
     */
    public function getPriority();
}
