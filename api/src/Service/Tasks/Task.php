<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Service\Tasks;

use Skel\Entity\EntityInterface;
use Skel\Service\Service;
use Skel\Entity\Tasks\TaskInterface as TaskModel;
use Skel\Repository\Tasks\TaskInterface as TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use InvalidArgumentException;
use Skel\Factory\Tasks\Task as TaskFactory;

/**
 * Task Service
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
final class Task extends Service implements TaskInterface
{
    /**
     * @var TaskRepository
     */
    private $tasks;

    /**
     * @param EntityManagerInterface $em
     * @param TaskRepository $tasks
     */
    public function __construct(EntityManagerInterface $em, TaskRepository $tasks)
    {
        parent::__construct($em);

        $this->tasks        = $tasks;
    }

    /**
     * @param  TaskModel $task
     * @return TaskModel
     */
    public function save(TaskModel $task)
    {
        $this->em->getConnection()->beginTransaction();

        try {
            $this->em->persist($task);
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (UniqueConstraintViolationException $ex) {
            $this->em->getConnection()->rollBack();

            $error = sprintf(
                'This task "%s" is already registered',
                $task->getTitle()
            );

            throw new InvalidArgumentException($error, 409, $ex);
        } catch (Exception $ex) {
            $this->em->getConnection()->rollBack();

            throw new InvalidArgumentException($ex->getMessage(), 500, $ex);
        }

        return $task;
    }

    /**
     * @param  TaskModel $task
     * @return boolean
     */
    public function delete(TaskModel $task)
    {
        $task->setStatus(EntityInterface::DELETED);

        $this->em->persist($task);
        $this->em->flush();

        return true;
    }

    /**
     * Update task
     *
     * @param TaskModel $task
     * @param Request $request
     * @return mixed
     */
    public function update(TaskModel $task, Request $request)
    {
        /* @var $update \Skel\Entity\Tasks\Task */
        $update = TaskFactory::update($task, $request);

        return $this->save($update);
    }

    /**
     * Find one by task id
     *
     * @param int $id  id
     * @return TaskModel
     */
    public function findById($id)
    {
        $task = $this->tasks->findById($id);

        return $task;
    }

    /**
     * List all tasks
     *
     * @return array|mixed
     */
    public function findAll()
    {
        $tasks = $this->tasks->findAll();

        return $tasks;
    }
}
