<?php
/**
 * This file is part of Skel system
 *
 * @license http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 */
namespace Skel\Service\Tasks;

use Skel\Entity\Tasks\TaskInterface as TaskModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * Task Service
 *
 * @author Romeu Mattos <romeu.smattos@gmail.com>
 */
interface TaskInterface
{
    /**
     * @const string
     */
    const NAME = 'tasks.task.service';

    /**
     * @param  TaskModel $task
     *
     * @return TaskModel
     */
    public function save(TaskModel $task);

    /**
     * @param  TaskModel $task
     *
     * @return boolean
     */
    public function delete(TaskModel $task);

    /**
     * Update task
     *
     * @param TaskModel $task
     * @param Request $request
     * @return mixed
     */
    public function update(TaskModel $task, Request $request);

    /**
     * Find one by task id
     *
     * @param int $id  id
     * @return TaskModel
     */
    public function findById($id);
}
