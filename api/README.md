Task API
=============

API REST desenvolvida utilizando o [silex-api-sekl](https://github.com/mrprompt/silex-api-skel/)

Instalação
==========
É necessário o PHP 5.6

## Extensões necessárias
- curl
- pdo
- reflection
- json
- xdebug (opcional)

## Instalação

MYSQL
Execute o comando:
```
 CREATE SCHEMA 'api' ;

```

Subindo os dados para banco
```
 ./vendor/bin/phing fixtures

```

## O arquivo config.yml
No arquivo *config.yml* na pasta *config* do projeto é onde ficam as configurações dos bancos

*Todos os parâmetros são obrigatórios.*

## Rodando localmente
Você pode utilizar o [servidor web embutido](http://php.net/manual/pt_BR/features.commandline.webserver.php) no [PHP](http://www.php.net)
para rodar localmente a API. Ou se preferir, configurar seu servidor web preferido apontando para a pasta *public*.
```
php composer.phar run
```

## Rodando em modo desenvolvimento
Rodar a API em modo de desenvolvimento, você deve definir a variável de ambiente *APPLICATION_ENV* com o valor *development*.
Caso a variável não esteja definida, o valor padrão é *production*.
Em modo de desenvolvimento, a aplicação irá mostrar todas as mensagens de erro e também de irá logar as mensagens de 
debug.
```
php composer.phar run
```

## Testando
Testes Unitários e Integração
```
./vendor/bin/phpunit
```

## Rotas
- Home
  - Url: /
  - Método: GET

- Task
  - Url: /tasks/[1-10]
  - Método: GET

- Task
  - Url: /tasks/
  - Método: POST

- Task
  - Url: /tasks/[1-10]
  - Método: DELETE

- Task
  - Url: /tasks/[1-10]
  - Método: PUT

