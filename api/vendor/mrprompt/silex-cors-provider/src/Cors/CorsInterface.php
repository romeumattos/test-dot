<?php
/**
 * This file is part of Enfants system
 *
 * @copyright Enfants
 * @license   proprietary
 */
namespace MrPrompt\Silex\Cors;

/**
 * CORS validation service interface
 *
 * @author Thiago Paes <mrprompt@gmail.com>
 * @author Marcel Araujo <admin@marcelaraujo.me>
 */
interface CorsInterface
{
    /**
     * @var string
     */
    const HTTP_CORS = 'http.cors.service';
}
