# silex-hmac-provider 
[![Build Status](https://travis-ci.org/mrprompt/silex-hmac-provider.png)](https://travis-ci.org/mrprompt/silex-hmac-provider) 
[![SensioLabsInsight](https://insight.sensiolabs.com/projects/7b8ed0fc-2f5a-4e6f-84fd-030430a3482e/mini.png)](https://insight.sensiolabs.com/projects/7b8ed0fc-2f5a-4e6f-84fd-030430a3482e)
[![Dependency Status](https://www.versioneye.com/user/projects/55ddde652383e9002500006d/badge.svg?style=flat)](https://www.versioneye.com/user/projects/55ddde652383e9002500006d)
[![Average time to resolve an issue](http://isitmaintained.com/badge/resolution/mrprompt/silex-hmac-provider.svg)](http://isitmaintained.com/project/mrprompt/silex-hmac-provider "Average time to resolve an issue")
[![Percentage of issues still open](http://isitmaintained.com/badge/open/mrprompt/silex-hmac-provider.svg)](http://isitmaintained.com/project/mrprompt/silex-hmac-provider "Percentage of issues still open")

# Install

```
composer require mrprompt/silex-hmac-provider
```

## Testing

Just run *phpunit* without parameters

```
phpunit
```
